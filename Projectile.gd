extends Node2D

var target: Node
var motion: Vector2

func init(pos: Vector2, motion: Vector2):
	self.position = pos
	self.target = target
	self.motion = motion
	self.rotate(motion.angle())
	self.get_node("Hitbox").connect("area_entered", self, "_on_area_entered")


func _process(delta):
	if not motion:
		return
	self.translate(motion * delta)


func _on_area_entered(area):
	print("body entered")
	if area.has_method("hit"):
		# if collider has hit
		area.hit()
	elif area.get_node("..").has_method("hit"):
		area.get_node("..").hit()
	else:
		print("projectile hit body with no hit() method.")
		print(area)
