extends Node2D

var time = 0
onready var root = get_tree().get_root().get_node("Root")

# Called when the node enters the scene tree for the first time.
func _process(delta):
	# set width of background bar
	var bg = self.get_node("background")
	var screenwidth = get_viewport().size.x
	bg.rect_size.x = screenwidth
	
	# set cursor position
	var time = root.time
	var proportion = time / 10
	var cursor = self.get_node("cursor")
	cursor.rect_position.x = proportion * screenwidth



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
