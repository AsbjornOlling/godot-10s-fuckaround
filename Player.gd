extends Node2D

export var speed = 300
var input_events = []
onready var root = get_tree().get_root().get_node("Root")
export var shoot_speed = 500
var projectile_scene = preload("res://Projectile.tscn")
var shoot_cooldown = 1
var shoot_countdown = 0

func spawn_projectile(shootvec):
	var projectile = projectile_scene.instance()
	projectile.init(self.position, shootvec)
	root.add_child(projectile)

func _physics_process(delta):
	var motion = motion_from_input(delta)
	self.translate(motion)
	
	var shootvec = shoot_vector()
	shoot_countdown -= delta
	if shoot_countdown < 0:
		shoot_countdown = 0
		if shootvec:
			# spawn projectile
			spawn_projectile(shootvec)
			
			shoot_countdown = shoot_cooldown
			
			input_events.append([root.time, "shoot", shootvec])
	
	# find old inputs
	var recorded_inputs = get_inputs_for_now(delta)
	for event in recorded_inputs:
		if event[1] == "move":
			self.translate(event[2])
		if event[1] == "shoot":
			spawn_projectile(event[2])
	
	# append this input
	input_events.append([root.time, "move", motion])


func shoot_vector():
	var shootvec = Vector2()
	var key_pressed = false
	if Input.is_action_pressed("shoot_right"):
		shootvec.x += 1
		key_pressed = true
	if Input.is_action_pressed("shoot_left"):
		shootvec.x -= 1
		key_pressed = true
	if Input.is_action_pressed("shoot_down"):
		shootvec.y += 1
		key_pressed = true
	if Input.is_action_pressed("shoot_up"):
		shootvec.y -= 1
		key_pressed = true
	if key_pressed:
		shootvec = shootvec.normalized() * self.shoot_speed
		return shootvec
	else:
		return null

func motion_from_input(delta):
	var motion = Vector2()
	if Input.is_action_pressed("move_right"):
		motion.x += 1
	if Input.is_action_pressed("move_left"):
		motion.x -= 1
	if Input.is_action_pressed("move_down"):
		motion.y += 1
	if Input.is_action_pressed("move_up"):
		motion.y -= 1
	motion = motion.normalized()
	motion *= self.speed * delta
	return motion


func get_inputs_for_now(delta):
	var now = self.root.time
	var results = []
	var search_intervals = []
	
	# generate up to two intervals, representing the time since last frame
	search_intervals.append([max(0, now - delta), now])
	if delta > now:
		search_intervals.append([10 - delta, 10])
	
	for event in self.input_events:
		var t = event[0]
		
		for interval in search_intervals:
			var n1 = interval[0]
			var n2 = interval[1]
			
			# check whether the event happened inside the interval
			if n1 < t and t < n2:
				results.append(event)
	return results

func hit():
	print("player hit")
	self.queue_free()
