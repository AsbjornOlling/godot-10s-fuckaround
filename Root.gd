extends Node

var time = 0

func new_enemy(x: int, y: int):
	var enemy = load("res://Enemy.tscn")
	var new = enemy.instance()
	new.position.x = x
	new.position.y = y
	return new
	

# called on every frame
func _process(delta):
	time += delta
	if time > 10:
		
		time -= 10
		var x = randi() % int(get_viewport().size.x)
		var y = randi() % int(get_viewport().size.y)
		add_child(new_enemy(x, y))
