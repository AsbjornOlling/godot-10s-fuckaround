extends Node2D

onready var player = get_node("../Player")
onready var root = get_tree().get_root().get_node("Root")
export var projectile_interval = 3
var projectile_countdown = projectile_interval
var projectile_scene = preload("res://Projectile.tscn")
export var shoot_speed: float = 500

func _process(delta):
	projectile_countdown -= delta
	if projectile_countdown < 0:
		# calculate shooting vector to player
		var vector_to_player: Vector2 = player.position - self.position
		var motion = vector_to_player.normalized() * shoot_speed

		# spawn projectile
		var projectile = projectile_scene.instance()
		projectile.init(self.position, motion)
		root.add_child(projectile)
		
		# reset counter
		projectile_countdown = projectile_interval

func hit():
	print("enemy hit!")
	self.queue_free()
